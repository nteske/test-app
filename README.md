# Why only one git commit?
![Shocked Pikachu](https://i.ibb.co/Gvbnm54/ezgif-2-1bd1b18a966d.gif)\
I usually commit everything in stages, but in this case, I felt more comfortable making only one commit.

# Test App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.6.

## Installing dependencies

To install dependencies necessary for building the projects please run `npm install` command.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Project structure

Simple description of how the project is structured.

## Core

Here I place side nav, about page, models, utils, and the rest of the core things that are being used in the app.

## Data

Mainly used to load and preserve data in the app, I split products and manufacturers into different services so they can be separately enhanced in the future.

## Products

Handling product pages, crud operations, etc.

## Shared - Why sharing some material modules and some not?

I decided to stick with the best practice of sharing modules that are being constantly used in the app (side nav) and inject others into different modules as needed.
Plus dialogs.

# Handling Errors - Why none?

I didn't see the point a point in spending time handling errors, the structure of projects is so simple, that I assume that there will only be small issues.

# Bonus points - Why did I decide to not take any?

If video games taught me anything, is that you are not going to collect all bonus points in your life.
