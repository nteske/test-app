import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './core/components/about/about.component';

import { ProductModule } from './product/product.module';

const routes: Routes = [
  { path: '', loadChildren:() => ProductModule },
  { path: 'about', component:AboutComponent },
  { path: '**', redirectTo: '/' } //simple 404
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
