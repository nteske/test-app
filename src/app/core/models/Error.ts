export interface IError{
    value: number;
    message: string;
}