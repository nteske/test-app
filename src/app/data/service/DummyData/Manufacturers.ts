import { IManufacturer } from 'src/app/core/models/Manufacturer';

export const manufacturers: IManufacturer[] = [
  {
    id: 'e363ce21-a563-4e49-8e71-88d1c1a3a75e',
    name: 'Hemofarm'
  },
  {
    id: 'e0f4342a-c6fc-4cf3-a1e8-9f22c4ce0a0f',
    name: 'Pfizer'
  },
  {
    id: '769e935f-6a29-48aa-8d1a-13f3ded6ebe6',
    name: 'Bayer'
  }
];
