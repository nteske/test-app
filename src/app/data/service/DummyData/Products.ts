import { IProduct } from "src/app/core/models/Product";
import { manufacturers } from "./Manufacturers";

export const products:IProduct[] = [
  {
    id: "07016283-588d-4fb8-875c-e61edf9ee591",
    name: "Famotidin",
    manufacturer: manufacturers[0], // I didn't see a point in making Enums here, I pretty much just randomly assigned manufacturers
    price: 20,
    expiryDate: new Date(2021,11,14),
  },
  {
    id: "39d7e6c5-4f73-416f-afa4-605d37a4fe97",
    name: "Panklav",
    manufacturer: manufacturers[2], 
    price: 50,
    expiryDate: new Date(2022,0,24),
  },
  {
    id: "11be066a-b6a7-4456-88e2-856fe9e7c3c8",
    name: "Amoksicilin",
    manufacturer: manufacturers[1],
    price: 100,
    expiryDate: new Date(2022,0,1),
  },
  {
    id: "47e6914c-88c6-45a2-97b6-c10a2df72a83",
    name: "Voltaren gel",
    manufacturer: manufacturers[1],
    price: 30,
    expiryDate: new Date(2023,0,6),
  },
  {
    id: "4e13a3c1-2cf8-4608-9b75-70850e02b941",
    name: "Probiotik",
    manufacturer: manufacturers[2], // I didn't see a point in making Enums here, I pretty much just randomly assigned manufacturers
    price: 10,
    expiryDate: new Date(2022,0,24),
  },
  {
    id: "5b63f960-9951-40cd-bb42-bb1ec127c7ba",
    name: "Pantenol",
    manufacturer: manufacturers[0],
    price: 25,
    expiryDate: new Date(2021,10,8),
  },
  {
    id: "15b7cb72-a812-489d-8c74-6fe527e36e1c",
    name: "Tothema kapi",
    manufacturer: manufacturers[2],
    price: 33,
    expiryDate: new Date(2023,0,16),
  },
  {
    id: "5b47d248-ebd9-46af-ba88-d1e04f6ba6a6",
    name: "Klometol",
    manufacturer: manufacturers[2],
    price: 92,
    expiryDate: new Date(2022,0,22),
  },
  {
    id: "b70eacc7-44d6-46b5-9d7c-c2d3c0d0969e",
    name: "Smecta",
    manufacturer: manufacturers[1],
    price: 44,
    expiryDate: new Date(2022,0,11),
  },
  {
    id: "d1816ce7-2eee-4a71-a9a7-cb2d3bb726db",
    name: "Bulardi",
    manufacturer: manufacturers[0], // I didn't see a point in making Enums here, I pretty much just randomly assigned manufacturers
    price: 31,
    expiryDate: new Date(2021,11,2),
  }
];