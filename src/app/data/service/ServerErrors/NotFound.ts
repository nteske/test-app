import { IError } from "src/app/core/models/Error";

export const notFound: IError = Object.freeze({
    value: 404,
    message: 'Not Found!'
});