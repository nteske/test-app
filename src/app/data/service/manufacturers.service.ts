import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { IManufacturer } from 'src/app/core/models/Manufacturer';

import { manufacturers } from './DummyData/Manufacturers';

@Injectable({
  providedIn: 'root'
})
export class ManufacturersService {

  constructor() { }

  getAll(): Observable<Array<IManufacturer>> {
    return of(manufacturers);
  }

}
