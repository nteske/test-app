import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

import { IProduct } from 'src/app/core/models/Product';

import { products } from './DummyData/Products';
import { notFound } from './ServerErrors/NotFound';

import Utils from 'src/app/core/utils/Utils';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  preserveState: IProduct[];

  constructor() {
    this.preserveState = products;
  }

  getAll(): Observable<Array<IProduct>> {
    return of(this.preserveState);
  }

  getById(id: string): Observable<IProduct> {
    /*     I decided to go with two iteration methods since I need to check if the object exists */
    let product = this.preserveState.find((prod) => prod.id == id);
    if (product) {
      return of(product);
    } else return throwError(notFound);
  }

  add(product: IProduct): Observable<IProduct> {
    product.id = Utils.uuidv4();
    this.preserveState.push(product);
    return of(product);
  }

  update(product: IProduct): Observable<IProduct> {
    let foundIndex = this.preserveState.findIndex(
      (prod) => prod.id == product.id
    );
    this.preserveState[foundIndex] = product;
    return of(product);
  }

  delete(id: string): Observable<IProduct> {
    /*     I decided to go with two iteration methods since I need to check if the object exists */
    let product = this.preserveState.find((prod) => prod.id == id);
    if (product) {
      this.preserveState = this.preserveState.filter((el) => el.id != id);
      return of(product);
    } else return throwError(notFound);
  }
}
