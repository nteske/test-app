import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { IProduct } from 'src/app/core/models/Product';
import { IManufacturer } from 'src/app/core/models/Manufacturer';

import { ProductHandleService } from '../../services/product-handle.service';

import Utils from 'src/app/core/utils/Utils';

@Component({
  selector: 'app-products-form',
  templateUrl: './products-form.component.html',
  styleUrls: ['./products-form.component.css'],
})
export class ProductsFormComponent implements OnInit {
  form: FormGroup;
  product: IProduct;
  manufacturers: IManufacturer[] = [];

  today: string = Utils.formatDate(new Date());

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private productHandleService: ProductHandleService
  ) {}

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.setForm(); //just create a form
    this.productHandleService
      .getAllManufacturers()
      .subscribe((manufacturers) => {
        this.manufacturers = manufacturers;
        if (!id) {
          if (this.manufacturers)
            this.form.patchValue({
              manufacturer: this.manufacturers[0].id,
            });
        } else {
          this.productHandleService.getProductById(id).subscribe((product) => {
            this.product = product;
            this.setProductValues();
          });
        }
      });
  }

  setForm():void {
    this.form = this.fb.group({
      name: new FormControl('', [
        Validators.minLength(3),
        Validators.maxLength(25),
      ]),
      manufacturer: new FormControl(null),
      price: new FormControl(1, Validators.min(1)),
      expiryDate: new FormControl(this.today),
    });
  }

  setProductValues():void {
    this.form.patchValue({
      ...this.product,
      manufacturer: this.product.manufacturer.id,
      expiryDate: Utils.formatDate(this.product.expiryDate),
    });
  }

  onSubmit():void {
    let submitProduct: IProduct = {
      ...this.form.value,
      manufacturer: this.manufacturers.find(
        (manuf) => manuf.id == this.form.value.manufacturer
      ),
      expiryDate: new Date(this.form.value.expiryDate),
    };
    if (this.product) {
      submitProduct.id = this.product.id;
      this.productHandleService.updateProduct(submitProduct).subscribe(() => {
        this.navigateHome();
      });
    } else
      this.productHandleService.addProduct(submitProduct).subscribe(() => {
        this.navigateHome();
      });
  }

  navigateHome():void {
    this.router.navigate(['/']);
  }
}
