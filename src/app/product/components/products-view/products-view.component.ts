import { Component, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from 'src/app/shared/components/delete-dialog/delete-dialog.component';

import { IProduct } from 'src/app/core/models/Product';

import { ProductDisplayService } from '../../services/product-display.service';

@Component({
  selector: 'app-products-view',
  templateUrl: './products-view.component.html',
  styleUrls: ['./products-view.component.css'],
})
export class ProductsViewComponent implements OnInit {
  products:IProduct[];

  constructor(private productDisplayService: ProductDisplayService,public dialog: MatDialog) {}

  ngOnInit(): void {
      this.productDisplayService.getAllProducts().subscribe((products) => this.products=products);
  }

  deleteProduct(id:string=""): void{
    let deleteDialog=this.dialog.open(DeleteDialogComponent,{data:"product"});
    deleteDialog.afterClosed().subscribe(result=>{
      if(result){
        this.productDisplayService.deleteProduct(id).subscribe(()=>{
          this.products=this.products.filter(product=>product.id != id);
        });
      }
    });
  }
}
