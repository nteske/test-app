import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductsFormComponent } from './components/products-form/products-form.component';
import { ProductsViewComponent } from './components/products-view/products-view.component';

const routes: Routes = [
  { path: '', component:ProductsViewComponent },
  { path: 'add', component:ProductsFormComponent },
  { path: 'edit/:id', component:ProductsFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
