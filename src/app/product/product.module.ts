import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProductRoutingModule } from './product-routing.module';

import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';

import { ProductsViewComponent } from './components/products-view/products-view.component';
import { ProductsFormComponent } from './components/products-form/products-form.component';

import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ProductsViewComponent,
    ProductsFormComponent,
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    SharedModule,
    FormsModule, 
    ReactiveFormsModule,
  ]
})
export class ProductModule { }
