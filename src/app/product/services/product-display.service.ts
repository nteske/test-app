import { Injectable } from '@angular/core';
import { Observable,throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

import { IProduct } from 'src/app/core/models/Product';

import { ProductService } from 'src/app/data/service/product.service';

@Injectable({
  providedIn: 'root'
})
export class ProductDisplayService {

  constructor(private productService:ProductService,private router:Router) { }

  getAllProducts(): Observable<Array<IProduct>>{
    return this.productService
    .getAll()
    .pipe(catchError((error: any) => throwError(error.statusText)));
  }

  deleteProduct(id:string):Observable<IProduct>{
    return this.productService.delete(id).pipe(catchError((error: any) => throwError(error.statusText)));
  }

}
