import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

import { IProduct } from 'src/app/core/models/Product';
import { IManufacturer } from 'src/app/core/models/Manufacturer';

import { ProductService } from 'src/app/data/service/product.service';
import { ManufacturersService } from 'src/app/data/service/manufacturers.service';

@Injectable({
  providedIn: 'root'
})
export class ProductHandleService {

  constructor(private productService:ProductService,private manufacturersService:ManufacturersService, private router:Router) { }

  getProductById(id:string): Observable<IProduct>{
    return this.productService
    .getById(id)
    .pipe(catchError((error: any) => throwError(error.statusText)));
  }


  getAllManufacturers(): Observable<Array<IManufacturer>>{
    return this.manufacturersService
    .getAll()
    .pipe(catchError((error: any) => throwError(error.statusText)));
  }

  addProduct(product:IProduct): Observable<IProduct>{
    return this.productService.add(product).pipe(catchError((error: any) => throwError(error.statusText)));
  }

  updateProduct(product:IProduct):Observable<IProduct>{
    return this.productService.update(product).pipe(catchError((error: any) => throwError(error.statusText)));
  }

}
