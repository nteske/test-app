import { NgModule } from '@angular/core';

import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [],
  imports: [],
  exports:[
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
  ]
})
export class MaterialModule { }
